// Requires
import Server from './server/server';

// MAIN
const main = async() => {
    const server = new Server();

    server.subirServidor();
}

main();