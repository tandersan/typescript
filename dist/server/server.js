"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Requires
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
const colors_1 = __importDefault(require("colors"));
const usuario_1 = __importDefault(require("../routes/usuario"));
const config_1 = __importDefault(require("../database/config"));
// Variables
dotenv_1.default.config();
colors_1.default.enable();
// MAIN
class Server {
    constructor() {
        this.apiPaths = {
            usuarios: '/api/usuarios'
        };
        this.app = (0, express_1.default)();
        this.puerto = process.env.PORT || '8000'; // Changing default port to 8000 when "env" is undefined - to catch if happens
        this.middlewares(); // Executing middlewares required before Routes
        this.routes(); // Api's Routes
        this.conectarBBDD(); //Database conexion
    }
    middlewares() {
        // Body parsing
        this.app.use(express_1.default.json());
        // Public folder
        this.app.use(express_1.default.static('public'));
    }
    routes() {
        this.app.use(this.apiPaths.usuarios, usuario_1.default);
    }
    conectarBBDD() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield config_1.default.authenticate();
                console.log(`MySQL:\\> Connected to Database : ${(process.env.NAME_DB).yellow}\n`);
            }
            catch (dbError) {
                if (dbError instanceof Error) {
                    console.log('\nERROR =============================================================================================================================================='.red);
                    console.log(dbError.message);
                }
            }
        });
    }
    subirServidor() {
        this.app.listen(this.puerto, () => {
            console.log(`\nServer On Line\nListening port : ${(this.puerto + '').yellow}`);
        });
    }
}
exports.default = Server;
//# sourceMappingURL=server.js.map