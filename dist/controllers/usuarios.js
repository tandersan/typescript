"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUsuario = exports.putUsuario = exports.postUsuario = exports.getUsuario = exports.getUsuarios = void 0;
const usuario_1 = __importDefault(require("../models/usuario"));
// MAIN
// Get All Users
const getUsuarios = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const usuarios = yield usuario_1.default.findAll();
        res.json({
            status: 'ok',
            usuarios
        });
    }
    catch (dbError) {
        if (dbError instanceof Error) {
            res.json({
                status: 'error',
                message: dbError.message,
                error_details: dbError
            });
        }
    }
});
exports.getUsuarios = getUsuarios;
// Get ONE User
const getUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const usuario = yield usuario_1.default.findByPk(id);
        if (usuario) {
            res.json({
                status: 'ok',
                usuario
            });
        }
        else {
            return res.status(404).json({
                status: 'error',
                message: `User ${id} doesn't exists`
            });
        }
    }
    catch (dbError) {
        if (dbError instanceof Error) {
            res.status(500).json({
                status: 'error',
                message: dbError.message,
                error_details: dbError
            });
        }
    }
});
exports.getUsuario = getUsuario;
// Add ONE User
const postUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    try {
        const usuario = yield usuario_1.default.create(body);
        res.json({
            status: 'ok',
            usuario
        });
    }
    catch (dbError) {
        if (dbError instanceof Error) {
            res.status(500).json({
                status: 'error',
                message: dbError.message,
                error_details: dbError
            });
        }
    }
});
exports.postUsuario = postUsuario;
// Update ONE User
const putUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { body } = req;
    try {
        const existeUsuario = yield usuario_1.default.findByPk(id);
        if (existeUsuario) {
            yield existeUsuario.update(body);
            res.json({
                status: 'ok',
                existeUsuario
            });
        }
        else {
            return res.status(404).json({
                status: 'error',
                message: `User ${id} doesn't exists`
            });
        }
    }
    catch (dbError) {
        if (dbError instanceof Error) {
            res.status(500).json({
                status: 'error',
                message: dbError.message,
                error_details: dbError
            });
        }
    }
});
exports.putUsuario = putUsuario;
// Delete ONE User
const deleteUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const existeUsuario = yield usuario_1.default.findByPk(id);
        if (existeUsuario) {
            yield existeUsuario.update({ estado: 0 });
            res.json({
                status: 'ok',
                message: `User id = ${id} disabled`
            });
        }
        else {
            return res.status(404).json({
                status: 'error',
                message: `User ${id} doesn't exists`
            });
        }
    }
    catch (dbError) {
        if (dbError instanceof Error) {
            res.status(500).json({
                status: 'error',
                message: dbError.message,
                error_details: dbError
            });
        }
    }
});
exports.deleteUsuario = deleteUsuario;
//# sourceMappingURL=usuarios.js.map