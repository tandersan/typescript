"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Requires
const dotenv_1 = __importDefault(require("dotenv"));
const sequelize_1 = require("sequelize");
// Variables
dotenv_1.default.config();
let opcionesDB;
// MAIN
opcionesDB = {
    host: process.env.SERVER_HOST,
    dialect: process.env.NAME_APP_DB,
    logging: false,
    define: {
        timestamps: false // Avoid createdAt or updateddAt error
    }
};
const dbConexion = new sequelize_1.Sequelize(process.env.NAME_DB, process.env.USER_DB, process.env.PASS_DB, opcionesDB);
exports.default = dbConexion;
//# sourceMappingURL=config.js.map