namespace NodeJS {
    interface ProcessEnv {
        PORT: string;
        USER_DB: string;
        PASS_DB: string;
        NAME_APP_DB: string;
        NAME_DB: string;        
        DB_HOST: string;
    }
  }