// Requires
import {Request, Response} from 'express';
import Usuario from '../models/usuario';

// MAIN
// Get All Users
export const getUsuarios = async (req: Request, res: Response) => {
    try {
        const usuarios = await Usuario.findAll();

        res.json({
            status: 'ok',
            usuarios
        })
    } catch(dbError) {
        if (dbError instanceof Error) { 
            res.json({
                status: 'error',
                message: dbError.message,
                error_details: dbError
            })
        }
    }
}

// Get ONE User
export const getUsuario = async (req: Request, res: Response) => {
    const {id} = req.params;

    try {
        const usuario = await Usuario.findByPk(id);

        if (usuario) {
            res.json({
                status: 'ok',
                usuario
            })
        } else {
            return res.status(404).json({
                status: 'error',
                message: `User ${id} doesn't exists`
            })
        }

    } catch(dbError) {
        if (dbError instanceof Error) { 
            res.status(500).json({
                status: 'error',
                message: dbError.message,
                error_details: dbError
            })
        }
    }
}

// Add ONE User
export const postUsuario = async (req: Request, res: Response) => {
    const {body} = req;

    try {
        const usuario = await Usuario.create(body);

        res.json({
            status: 'ok',
            usuario
        })
    } catch(dbError) {
        if (dbError instanceof Error) { 
            res.status(500).json({
                status: 'error',
                message: dbError.message,
                error_details: dbError  
            })
        }
    }
}

// Update ONE User
export const putUsuario = async (req: Request, res: Response) => {
    const {id} = req.params;
    const {body} = req;

    try {
        const existeUsuario = await Usuario.findByPk(id);

        if (existeUsuario) {
            await existeUsuario.update(body);

            res.json({
                status: 'ok',
                existeUsuario
            })
        } else {
            return res.status(404).json({
                status: 'error',
                message: `User ${id} doesn't exists`
            })
        }
    } catch(dbError) {
        if (dbError instanceof Error) { 
            res.status(500).json({
                status: 'error',
                message: dbError.message,
                error_details: dbError  
            })
        }
    }
}

// Delete ONE User
export const deleteUsuario = async (req: Request, res: Response) => {
    const {id} = req.params;

    try {
        const existeUsuario = await Usuario.findByPk(id);

        if (existeUsuario) {
            await existeUsuario.update({estado: 0});

            res.json({
                status: 'ok',
                message: `User id = ${id} disabled`
            })
        } else {
            return res.status(404).json({
                status: 'error',
                message: `User ${id} doesn't exists`
            })
        }
    } catch(dbError) {
        if (dbError instanceof Error) { 
            res.status(500).json({
                status: 'error',
                message: dbError.message,
                error_details: dbError  
            })
        }
    }
}