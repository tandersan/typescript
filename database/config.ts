// Requires
import dotenv from 'dotenv';
import { Sequelize } from 'sequelize';

// Variables
dotenv.config();
let opcionesDB: object;

// MAIN
opcionesDB = {
    host: process.env.SERVER_HOST,
    dialect: process.env.NAME_APP_DB,
    logging: false,
    define: {
        timestamps: false // Avoid createdAt or updateddAt error
    }
}

const dbConexion = new Sequelize(process.env.NAME_DB, process.env.USER_DB, process.env.PASS_DB, opcionesDB);

export default dbConexion;