// Requires
import express, {Application} from 'express';
import dotenv from 'dotenv';
import colors from 'colors';
import userRoutes from '../routes/usuario';
import dbConexion from '../database/config';

// Variables
dotenv.config();
colors.enable();

// MAIN
class Server {
    private app: Application;
    private puerto: string;
    private apiPaths = {
        usuarios: '/api/usuarios'
    }

    constructor() {
        this.app = express();
        this.puerto = process.env.PORT || '8000'; // Changing default port to 8000 when "env" is undefined - to catch if happens
        this.middlewares(); // Executing middlewares required before Routes
        this.routes(); // Api's Routes
        this.conectarBBDD(); //Database conexion
    }

    middlewares() {
        // Body parsing
        this.app.use(express.json());

        // Public folder
        this.app.use(express.static('public'));
    }

    routes() {
        this.app.use(this.apiPaths.usuarios, userRoutes);
    }

    async conectarBBDD() {
        try {
            await dbConexion.authenticate();
            console.log (`MySQL:\\> Connected to Database : ${(process.env.NAME_DB).yellow}\n`);
        } catch(dbError) {
            if (dbError instanceof Error) {
                console.log('\nERROR =============================================================================================================================================='.red);
                console.log(dbError.message);
            }
        }
    }

    subirServidor() {
        this.app.listen(this.puerto, () => {
            console.log(`\nServer On Line\nListening port : ${(this.puerto + '').yellow}`);
        });
    }
}

export default Server;