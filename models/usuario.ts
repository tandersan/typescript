// Requires
import { DataTypes } from 'sequelize';
import dbConexion from '../database/config';

// MAIN
const Usuario = dbConexion.define('Usuario', {
    nombre: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    },
    estado: {
        type: DataTypes.BOOLEAN
    }
})

export default Usuario; 